const { AkairoClient, CommandHandler, InhibitorHandler, ListenerHandler } = require('discord-akairo')
const logger = require('./util/logger')
const config = require('../config')
const Redis = require('./util/db')
const db = new Redis(config.redis)
// eslint-disable-next-line no-unused-vars
const util = require('./util/util')

class WoomyClient extends AkairoClient {
  constructor () {
    super({
      ownerID: config.ownerIDs
    }, {
      ws: {
        intents: [
          'GUILDS',
          'GUILD_MEMBERS',
          'GUILD_VOICE_STATES',
          'GUILD_PRESENCES',
          'GUILD_MESSAGES',
          'GUILD_MESSAGE_REACTIONS',
          'DIRECT_MESSAGES',
          'DIRECT_MESSAGE_REACTIONS'
        ]
      }
    })

    this.logger = logger
    this.config = config
    this.db = db

    this.commandHandler = new CommandHandler(this, {
      directory: './commands/',
      prefix: config.defaultPrefix,
      defaultCooldown: 2000,
      blockBots: true,
      allowMention: true,
      automateCategories: true
    })

    this.inhibitorHandler = new InhibitorHandler(this, {
      directory: './inhibitors/'
    })

    this.listenerHandler = new ListenerHandler(this, {
      directory: './listeners/'
    })

    this.commandHandler.useListenerHandler(this.listenerHandler)
    this.commandHandler.useInhibitorHandler(this.inhibitorHandler)

    this.commandHandler.loadAll()
    this.logger.log('Command handler loaded.')
    this.inhibitorHandler.loadAll()
    this.listenerHandler.loadAll()
  }
}

const client = new WoomyClient()
client.login(config.token)
