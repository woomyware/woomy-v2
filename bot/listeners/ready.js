const { Listener } = require('discord-akairo')

class ReadyListener extends Listener {
  constructor () {
    super('ready', {
      emitter: 'client',
      event: 'ready'
    })
  }

  exec () {
    this.client.user.setActivity('akairo is kinda cool')
    this.client.logger.ready(`Connected to Discord as ${this.client.user.tag}`)
  }
}

module.exports = ReadyListener
