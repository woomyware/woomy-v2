const { Command } = require('discord-akairo')

class PingCommand extends Command {
  constructor () {
    super('ping', {
      aliases: ['ping']
    })
  }

  async exec (message) {
    const msg = await message.channel.send('Pinging...')
    msg.edit(
      `Pong! \`${msg.createdTimestamp - message.createdTimestamp}ms\` (💗 \`${Math.round(this.client.ws.ping)}ms\`)`
    )
  }
}

module.exports = PingCommand
